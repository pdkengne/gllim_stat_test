#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Question 2


library("xLLiM")

# 

# Simulation of pairs (X_n, Y_n) for n=1,2, ..., N

# X and Y are two univariate random variables

# sample size

N<- 1000

# Number of regions

K<- 3


# Covariance of X per region

Sigma_1<- 0.2
Sigma_2<- 0.2
Sigma_3<- 0.2

# Vector of covariances with respect to X

Sigma<- c(Sigma_1, Sigma_2, Sigma_3)


# Covariance of Y per region

Gamma_1<- 0.2
Gamma_2<- 0.2
Gamma_3<- 0.2

# Vector of covariances with respect to Y

Gamma<- c(Gamma_1, Gamma_2, Gamma_3)


# Prior probabilities per region

Pi_1<- 1/K
Pi_2<- 1/K
Pi_3<- 1/K

# Vector of prior probability with respect to region

Pi<- c(Pi_1, Pi_2, Pi_3)


# Average values of Y per region

c_1<- 1
c_2<- 2
c_3<- 3

# Vector of average values with respect to Y

C<- c(c_1, c_2, c_3)


# Slopes of the lines explaining X by Y per region

A_1<- -3
A_2<- 1
A_3<- +3


# Vector of slopes of the lines explaining X by Y with respect to regions

A<- c(A_1, A_2, A_3)


# Intercepts of the lines explaining X by Y per region

b_1<- -5
b_2<- 0
b_3<- +5

# # Vector of intercepts of the lines explaining X by Y with respect to regions 

B<- c(b_1, b_2, b_3)



# Seed value for reproduction

set.seed(123)

# Simulation region labels 1, 2 or 3

Z<- sample(x = c(1:K), size = N, replace = TRUE, prob = Pi)

# Simulation of Y given the region

Y<- sapply(1:N, function(k) rnorm(n = 1, mean = C[Z[k]], sd = Gamma[Z[k]]))

# Simulation of X given the region and Y

X<- sapply(1:N, function(k) rnorm(n= 1, mean = A[Z[k]]*Y[k] + B[Z[k]], sd = Sigma[Z[k]]))


# Total number of parameters

P<- (length(Pi) - 1) + length(C) + length(Gamma) + length(Sigma) + length(A) + length(B)

P

# The total number of parameters is 17

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
